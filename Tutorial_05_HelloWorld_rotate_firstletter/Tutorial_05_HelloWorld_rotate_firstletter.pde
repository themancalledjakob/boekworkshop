import geomerative.*;

// Declare the objects we are going to use, so that they are accesible from setup() and from draw()
RFont f;
RShape text_original;
RShape text_transformed;

void setup(){
  // Initilaize the sketch
  size(600,400);
  frameRate(24);

  // VERY IMPORTANT: Allways initialize the library in the setup
  RG.init(this);

  // Choice of colors
  background(0);
  fill(255,255,125,125);
  stroke(0);
  noStroke();
  
  //  Load the font file we want to use (the file must be in the data folder in the sketch floder), with the size 60 and the alignment CENTER
  text_original = RG.getText("Hello world!", "FreeSans.ttf", 72, CENTER);
  // Enable smoothing
  smooth();
}

void draw(){
  // Clean frame
  background(0);
  
  // Set the origin to draw in the middle of the sketch
  translate(width/2, height/2);
  text_transformed = new RShape(text_original);
  // Transform at each frame the first letter with a PI/20 radians 
  // rotation around the center of the first letter's center
  for (int letter = 0; letter < text_transformed.children.length; letter++){
    RPoint[][] pathsAndHandles = text_transformed.children[letter].getHandlesInPaths();
    RPoint[][] pathsAndHandles_original = text_original.children[letter].getHandlesInPaths();
    for (int path = 0; path < pathsAndHandles.length; path++){
      for (int handle = 0; handle < pathsAndHandles[path].length; handle++){
        pathsAndHandles[path][handle].x += random(10)-5;
        pathsAndHandles[path][handle].y += random(10)-5;
      }
    }
  }
  
  // Draw the group of shapes representing "Hola mundo!" on the PGraphics canvas g (which is the default canvas of the sketch)
  
  fill(255,0,255,255);
  text_original.draw();
  fill(255);
  text_transformed.draw();
}
